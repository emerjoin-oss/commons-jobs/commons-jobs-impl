## Commons-jobs-impl
Implementation of commons-jobs-api

# Maven Coordinates
## Repository
Emerjoin OSS repository
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>maven</name>
    <url>https://pkg.emerjoin.org/oss</url>
</repository>
```

## Artifact
Latest version
```xml
<dependency>
    <groupId>org.emerjoin.commons</groupId>
    <artifactId>commons-jobs-impl</artifactId>
    <version>1.0.0-final</version>
</dependency>
```