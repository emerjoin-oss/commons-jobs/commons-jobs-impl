package org.emerjoin.commons.jobs.impl;

import org.emerjoin.commons.jobs.Job;
import org.emerjoin.commons.jobs.JobsException;

public class JobNotScheduledException extends JobsException {

    public JobNotScheduledException(Class<? extends Job> jobType) {
        super("Job type is not Auto-Scheduled: "+ jobType.getCanonicalName());
    }

}
