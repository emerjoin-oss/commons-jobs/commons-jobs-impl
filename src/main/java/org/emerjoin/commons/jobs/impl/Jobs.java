package org.emerjoin.commons.jobs.impl;

import org.apache.deltaspike.core.api.config.ConfigResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.emerjoin.commons.data.mapping.Converters;
import org.emerjoin.commons.data.mapping.TypedDataMap;
import org.emerjoin.commons.data.mapping.proxing.DataProxy;
import org.emerjoin.commons.jobs.*;
import org.emerjoin.commons.jobs.impl.cdi.CdiJobExecutionContext;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class Jobs implements JobsProviderContext, JobsContext {

    private static final Logger LOGGER = LogManager.getLogger(Jobs.class);
    private static Map<String,JobTypeInfo> JOB_TYPE_INFO_MAP = new HashMap<>();
    private static List<String> JOB_NAMES_LIST = new ArrayList<>();

    @Inject
    private JobsProvider jobsProvider;

    @Inject
    private Event<BeforeJobExecution> beforeJobExecutionEvent;

    @Inject
    private Event<AfterJobExecution> afterJobExecutionEvent;

    @Inject
    private Event<JobsBootstrap> jobBootstrapEvent;

    private Converters converters;

    private final List<JobInstance> jobInstanceList = Collections.synchronizedList(new ArrayList<>());

    protected static void jobTypeDiscovered(Class<? extends Job> jobType){
        JobInfo jobInfo = jobType.getAnnotation(JobInfo.class);
        AutoSchedule autoSchedule = jobType.getAnnotation(AutoSchedule.class);
        if(autoSchedule!=null){
            LOGGER.info("Registering Job Type {} with @Auto-Schedule expression-key={} and default-expression={}",jobType.getCanonicalName(),
                    autoSchedule.expressionKey(),autoSchedule.
                            defaultExpression());
        }else{
            LOGGER.info("Registering Job Type {} with NO @Auto-Schedule",jobType.getCanonicalName());
        }
        Class<? extends JobParamObject> jobParamObjectType = null;
        if(ParameterizedJob.class.isAssignableFrom(jobType)){
            jobParamObjectType = (Class<? extends JobParamObject>) getParameterizedJobType(jobType)
                    .getActualTypeArguments()[1];
        }
        JobTypeInfo jobTypeInfo = new JobTypeInfo(jobType, jobInfo, autoSchedule, jobParamObjectType);
        if(JOB_NAMES_LIST.contains(jobInfo.name())){
            throw new DuplicateJobNameException(jobInfo.
                    name());
        }
        JOB_NAMES_LIST.add(jobInfo.name());
        JOB_TYPE_INFO_MAP.put(jobType.getCanonicalName(),jobTypeInfo);
    }

    private static ParameterizedType getParameterizedJobType(Class<? extends Job> jobType){
        Type[] genericInterfaces = jobType.getGenericInterfaces();
        for (Type type: genericInterfaces){
            if(type.getTypeName().startsWith(ParameterizedJob.class.getCanonicalName())){
                return (ParameterizedType) type;
            }
        }
        throw new JobsException(String.format("could not find %s generic interface on Job Type: %s",ParameterizedJob.class.getSimpleName(),
                jobType.getCanonicalName()));
    }


    public void initialize(@Observes @Initialized(ApplicationScoped.class) Object event){
        LOGGER.info("Bootstrapping Jobs...");
        JobsBootstrapImpl bootstrapEventImpl = new JobsBootstrapImpl();
        LOGGER.info("Firing {} event...",JobsBootstrap.class.getSimpleName());
        jobBootstrapEvent.fire(bootstrapEventImpl);
        LOGGER.info("{} event fired...",JobsBootstrap.class.getSimpleName());
        this.converters = bootstrapEventImpl.getConverters();
        LOGGER.info("Initializing provider");
        this.jobsProvider.initialize(this);
        LOGGER.info("Auto-scheduling Jobs...");
        if(JOB_TYPE_INFO_MAP.size()==0){
            LOGGER.warn("There are no Jobs discovered. Nothing to schedule");
            return;
        }
        for(JobTypeInfo jobTypeInfo: JOB_TYPE_INFO_MAP.values()){
            String expression = getJobExpression(jobTypeInfo);
            LOGGER.info("Scheduling Job {} with expression = {}",jobTypeInfo.getType().getCanonicalName(),
                    expression);
            this.jobsProvider.schedule(jobTypeInfo.getType(),expression);
        }
        LOGGER.info("All {} Jobs scheduled",JOB_TYPE_INFO_MAP.size());
    }


    @PreDestroy
    public void shutdown(){
        LOGGER.info("Shutting down Jobs...");
        LOGGER.info("Clearing Job Type Info map...");
        JOB_TYPE_INFO_MAP.clear();
        JOB_NAMES_LIST.clear();
        LOGGER.info("Tearing down provider...");
        jobsProvider.teardown();
    }

    private String getJobExpression(JobTypeInfo jobTypeInfo){
        AutoSchedule autoSchedule = jobTypeInfo.getAutoSchedule();
        if(autoSchedule==null)
            throw new JobNotScheduledException(jobTypeInfo.getType());
        return ConfigResolver.resolve(autoSchedule.
                expressionKey())
                .withDefault(autoSchedule.defaultExpression())
                .getValue();
    }

    @Override
    public void executeScheduled(Class<? extends Job> jobType, JobExecutionId executionId) {
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
        if(executionId==null)
            throw new IllegalArgumentException("executionId must not be null");
        JobTypeInfo jobTypeInfo = getTypeInfo(jobType);
        AutoSchedule autoSchedule = jobTypeInfo.getAutoSchedule();
        if(autoSchedule==null)
            throw new JobNotScheduledException(jobType);
        JobParamsProvider jobParamsProvider = CDI.current().select(autoSchedule.paramsProvider()).get();
        JobParams params = jobParamsProvider.provide();
        this.execute(jobType,executionId,params.values());
    }

    @Override
    public void execute(Class<? extends Job> jobType, JobExecutionId executionId) {
        this.execute(jobType,executionId, Collections.emptyMap());
    }

    public void execute(Class<? extends Job> jobType, JobExecutionId executionId, Map<String,Object> params){
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
        if(executionId==null)
            throw new IllegalArgumentException("executionId must not be null");
        if(params==null)
            throw new IllegalArgumentException("params must not be null");
        JobTypeInfo jobTypeInfo = getTypeInfo(jobType);
        JobExecutionContextImpl context = new JobExecutionContextImpl(executionId);
        ThreadContext.put(Constants.KEY_JOB_EXECUTION_ID,executionId.value());
        CdiJobExecutionContext.startNew(context);
        JobInstanceImpl jobInstance = new JobInstanceImpl(executionId, jobTypeInfo,context);
        try{
            this.jobInstanceList.add(jobInstance);
            if(jobTypeInfo.isParameterized()){
                this.executeParameterizedJob(jobTypeInfo,executionId,
                        params);
            }else{
                this.executeSimpleJob(jobTypeInfo,executionId);
            }
        }finally {
            CdiJobExecutionContext.endCurrent();
            ThreadContext.remove(Constants.KEY_JOB_EXECUTION_ID);
            jobInstance.executionComplete();
            this.jobInstanceList.remove(jobInstance);
        }
    }

    @Override
    public JobTypeInfo getTypeInfo(Class<? extends Job> type) {
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        JobTypeInfo jobTypeInfo = JOB_TYPE_INFO_MAP.get(type.getCanonicalName());
        if(jobTypeInfo==null)
            throw new UnknownJobTypeException(type);
        return jobTypeInfo;
    }

    private BeforeJobExecution beforeJobExecution(Class<? extends Job> jobType, JobExecutionId executionId, Map<String,Object> map){
        ThreadContext.put(Constants.KEY_JOB_EXECUTION_ID,executionId.value());
        LOGGER.debug("Firing {} event",BeforeJobExecution.class.getSimpleName());
        JobParams jobParams = JobParams.fromMap(map);
        BeforeJobExecution event = new BeforeJobExecution(jobType,executionId,jobParams);
        this.beforeJobExecutionEvent.fire(event);
        LOGGER.info("{} event fired",BeforeJobExecution.class.getSimpleName());
        LOGGER.info("Starting Job execution");
        return event;
    }

    private void afterJobExecution(Class<? extends Job> jobType, JobExecutionId executionId, JobResult result){
        LOGGER.info("Job execution complete successfully");
        LOGGER.debug("Firing {} event",AfterJobExecution.class.getSimpleName());
        this.afterJobExecutionEvent.fire(new AfterJobExecution(jobType,executionId, result));
        LOGGER.info("{} event fired",AfterJobExecution.class.getSimpleName());
    }

    private void afterJobExecution(Class<? extends Job> jobType, JobExecutionId executionId, RuntimeException exception){
        LOGGER.error("Job execution failed");
        LOGGER.debug("Firing {} event",AfterJobExecution.class.getSimpleName());
        this.afterJobExecutionEvent.fire(new AfterJobExecution(jobType,executionId, exception));
        LOGGER.info("{} event fired",AfterJobExecution.class.getSimpleName());
    }

    private void executeSimpleJob(JobTypeInfo jobTypeInfo, JobExecutionId executionId){
        Class<? extends Job> jobType = jobTypeInfo.getType();
        SimpleJob simpleJob = (SimpleJob) getJobInstance(jobType);
        BeforeJobExecution beforeJobExecution =  this.beforeJobExecution(jobType,executionId,new HashMap<>());
        if(beforeJobExecution.isHalt()){
            LOGGER.info("Parameterized Job execution was halt before execution");
            return;
        }
        try {
            JobResult<?> jobResult = simpleJob.execute();
            afterJobExecution(jobType,executionId,jobResult);
        }catch (RuntimeException ex){
            this.afterJobExecution(jobType,executionId,ex);
            throw new JobExecutionException(jobType,ex);
        }

    }

    private void executeParameterizedJob(JobTypeInfo jobTypeInfo, JobExecutionId executionId, Map<String,Object> params){
        Class<? extends Job> jobType = jobTypeInfo.getType();
        ParameterizedJob parameterizedJob = (ParameterizedJob) getJobInstance(jobType);
        ThreadContext.put(Constants.KEY_JOB_EXECUTION_ID,executionId.value());
        BeforeJobExecution beforeJobExecution = this.beforeJobExecution(jobType,executionId,params);
        if(beforeJobExecution.isHalt()){
            LOGGER.info("Parameterized Job execution was halt before execution");
            return;
        }
        try {
            JobResult<?> jobResult = parameterizedJob.execute(makeParamObject(jobTypeInfo.getJobParamType(),
                    params));
            this.afterJobExecution(jobType,executionId,jobResult);
        }catch (RuntimeException ex){
            this.afterJobExecution(jobType,executionId,ex);
            throw new JobExecutionException(jobType,ex);
        }
    }

    private JobParamObject makeParamObject(Class<? extends JobParamObject> jobParamType, Map<String,Object> params){
        TypedDataMap typedDataMap = new TypedDataMap(params,converters);
        return DataProxy.make(jobParamType,
                typedDataMap);
    }

    private Job getJobInstance(Class<? extends Job> jobType){
        return CDI.current().select(jobType).get();
    }


    @Override
    public Collection<JobTypeInfo> listJobs() throws JobsException {
        return Collections.unmodifiableCollection(JOB_TYPE_INFO_MAP.
                values());
    }

    @Override
    public Collection<JobInstance> listRunning() throws JobsException {
        return Collections.unmodifiableList(
                jobInstanceList);
    }

    @Override
    public Collection<JobInstance> listRunning(Class<? extends Job> jobType) throws JobsException {
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
        return Collections.unmodifiableCollection(
                jobInstanceList.stream().filter(it -> it.getTypeInfo().getType() == jobType).collect(Collectors.
                        toList()));
    }

    @Override
    public void rescheduleAll() throws JobsException {
        for(JobTypeInfo jobTypeInfo: JOB_TYPE_INFO_MAP.values()){
            this.reschedule(jobTypeInfo.getType());
        }
    }

    @Override
    public void reschedule(Class<? extends Job> jobType) throws JobsException {
        this.checkJobType(jobType);
        JobTypeInfo jobTypeInfo = getTypeInfo(jobType);
        String expression = getJobExpression(jobTypeInfo);
        LOGGER.info("Re-scheduling Job {} with expression = {}",jobTypeInfo.getType().getCanonicalName(),
                expression);
        JobSchedule jobSchedule = jobsProvider.getJobSchedule(jobType);
        jobSchedule.change(expression);
    }

    @Override
    public void execute(Class<? extends Job> jobType, JobParams jobParams) throws JobsException {
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
        if(jobParams==null)
            throw new IllegalArgumentException("jobParams must not be null");
        JobExecutionId executionId = new JobExecutionId(UUID.randomUUID().toString());
        if(ParameterizedJob.class.isAssignableFrom(jobType)){
            this.executeParameterizedJob(getTypeInfo(jobType),executionId,jobParams.
                    values());
        }else this.executeSimpleJob(getTypeInfo(jobType),executionId);
    }

    @Override
    public void execute(Class<? extends Job> jobType) throws JobsException {
        this.execute(jobType,JobParams.empty());
    }

    @Override
    public JobExecutionId trigger(Class<? extends Job> jobType, JobParams jobParams) throws JobsException {
        if(jobParams==null)
            throw new IllegalArgumentException("jobParams must not be null");
        return jobsProvider.execute(jobType,jobParams);
    }

    @Override
    public JobExecutionId trigger(Class<? extends Job> jobType) throws JobsException {
        return this.trigger(jobType,JobParams.empty());
    }

    @Override
    public Optional<JobTypeInfo> getJobTypeInfo(Class<? extends Job> jobType) throws JobsException {
        this.checkJobType(jobType);
        return Optional.ofNullable(JOB_TYPE_INFO_MAP.get(jobType.
                getCanonicalName()));
    }

    @Override
    public Optional<JobTypeInfo> getJobTypeInfoByName(String jobName) throws JobsException {
        if(jobName==null||jobName.isEmpty())
            throw new IllegalArgumentException("jobName must not be null nor empty");
        return JOB_TYPE_INFO_MAP.values().stream()
                .filter(it -> it.getInfo().name().equals(jobName))
                .findAny();
    }

    private void checkJobType(Class<? extends Job> jobType){
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
    }
}
