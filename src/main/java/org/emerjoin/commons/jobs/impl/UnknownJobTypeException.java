package org.emerjoin.commons.jobs.impl;

import org.emerjoin.commons.jobs.Job;
import org.emerjoin.commons.jobs.JobsException;

public class UnknownJobTypeException extends JobsException {

    public UnknownJobTypeException(Class<? extends Job> type) {
        super(String.format("The type %s is unknown to the Jobs Discovery Strategy",type.getCanonicalName()));
    }

}
