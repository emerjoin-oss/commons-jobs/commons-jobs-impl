package org.emerjoin.commons.jobs.impl;

import org.emerjoin.commons.jobs.JobExecutionContext;
import org.emerjoin.commons.jobs.JobExecutionId;

public class JobExecutionContextImpl implements JobExecutionContext {

    private boolean interruptionRequested = false;
    private JobExecutionId jobExecutionId;

    public JobExecutionContextImpl(JobExecutionId executionId){
        this.jobExecutionId = executionId;
    }

    @Override
    public JobExecutionId getExecutionId() {
        return this.jobExecutionId;
    }

    public void requestInterruption(){
        this.interruptionRequested = true;
    }

    @Override
    public boolean isInterruptionRequested() {
        return this.interruptionRequested;
    }

}
