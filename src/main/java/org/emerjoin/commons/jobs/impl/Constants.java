package org.emerjoin.commons.jobs.impl;

public final class Constants {

    public static final String KEY_JOB_EXECUTION_ID = "JobExecutionId";

}
