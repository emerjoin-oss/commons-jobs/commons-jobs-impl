package org.emerjoin.commons.jobs.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emerjoin.commons.jobs.Job;
import org.emerjoin.commons.jobs.JobExecutionScoped;
import org.emerjoin.commons.jobs.JobInfo;
import org.emerjoin.commons.jobs.impl.cdi.CdiJobExecutionContext;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.*;

public class JobsCDIExtension implements Extension {

    private static final Logger LOGGER = LogManager.getLogger(JobsCDIExtension.class);

    public void addScope(@Observes final BeforeBeanDiscovery event) {
        event.addScope(JobExecutionScoped.class, true, false);
    }

    public void registerJob(@Observes @WithAnnotations(JobInfo.class) final ProcessAnnotatedType<?> processAnnotatedType){
        Class<?> type = processAnnotatedType.getAnnotatedType().getJavaClass();
        if(Job.class.isAssignableFrom(type)){
            LOGGER.info("Job Type discovered: "+type.getCanonicalName());
            Jobs.jobTypeDiscovered((Class<? extends Job>) type);
        }else{
            LOGGER.warn("Non-Job type was annotated with @{}: {}. Type ignored",JobInfo.class.getSimpleName(),type.
                    getCanonicalName());
        }
    }

    public void registerContext(@Observes final AfterBeanDiscovery event) {
        event.addContext(new CdiJobExecutionContext());
    }

}
