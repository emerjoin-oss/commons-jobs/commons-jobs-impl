package org.emerjoin.commons.jobs.impl;

import org.emerjoin.commons.jobs.Job;
import org.emerjoin.commons.jobs.JobsException;

public class JobExecutionException extends JobsException {

    public JobExecutionException(Class<? extends Job> jobType, Throwable cause){
        super("Error found executing Job: "+jobType.getCanonicalName(),cause);
    }

    public JobExecutionException(String message) {
        super(message);
    }

    public JobExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

}
