package org.emerjoin.commons.jobs.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emerjoin.commons.data.mapping.Converters;
import org.emerjoin.commons.data.mapping.ValueConverter;
import org.emerjoin.commons.data.mapping.proxing.TypePair;
import org.emerjoin.commons.jobs.JobsBootstrap;

public class JobsBootstrapImpl implements JobsBootstrap {

    private Converters converters = Converters.defaults();

    private static final Logger LOGGER = LogManager.getLogger(JobsBootstrapImpl.class);

    @Override
    public <FromType, ToType> void registerConverter(Class<FromType> fromTypeClass, Class<ToType> toTypeClass, ValueConverter<ToType> valueConverter) {
        if(fromTypeClass==null)
            throw new IllegalArgumentException("fromTypeClass must not be null");
        if(toTypeClass==null)
            throw new IllegalArgumentException("toTypeClass must not be null");
        if(valueConverter==null)
            throw new IllegalArgumentException("valueConverter must not be null");
        TypePair typePair = new TypePair(fromTypeClass,fromTypeClass);
        LOGGER.info("Registering Value Converter {}",typePair);
        converters.register(typePair,valueConverter);
    }

    public Converters getConverters() {
        return converters;
    }
}
