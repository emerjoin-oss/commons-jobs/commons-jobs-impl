package org.emerjoin.commons.jobs.impl.cdi;

import java.util.Optional;
import java.util.Stack;

public class ThreadState {

    private static final ThreadLocal<ThreadState> STATE_THREAD_LOCAL = new ThreadLocal<>();

    private Stack<CdiJobExecutionContextInstance> contextInstances = new Stack<>();

    private ThreadState(){

    }

    public boolean isContextActive(){

        return !contextInstances.isEmpty();

    }

    Optional<CdiJobExecutionContextInstance> currentContext(){
        if(contextInstances.isEmpty())
            return Optional.empty();
        return Optional.of(contextInstances
                .peek());
    }

    int countContexts(){
        return contextInstances.size();
    }

    CdiJobExecutionContextInstance startNewContext(){
        CdiJobExecutionContextInstance contextInstance = new CdiJobExecutionContextInstance();
        contextInstances.add(contextInstance);
        return contextInstance;
    }

    void endCurrentContext(){
        if(contextInstances.isEmpty())
            return;
        CdiJobExecutionContextInstance instance = contextInstances.pop();
        instance.destroy();
    }

    static ThreadState current(){
        ThreadState state = STATE_THREAD_LOCAL.get();
        if(state==null){
            state = new ThreadState();
            STATE_THREAD_LOCAL.set(state);
        }
        return state;
    }

}
