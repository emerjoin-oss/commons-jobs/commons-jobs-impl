package org.emerjoin.commons.jobs.impl;

import org.emerjoin.commons.jobs.JobExecutionId;
import org.emerjoin.commons.jobs.JobInstance;
import org.emerjoin.commons.jobs.JobTypeInfo;

public class JobInstanceImpl implements JobInstance {

    private JobExecutionId jobExecutionId;
    private JobTypeInfo jobTypeInfo;
    private JobExecutionContextImpl executionContext;
    private boolean running = false;

    JobInstanceImpl(JobExecutionId jobExecutionId, JobTypeInfo jobTypeInfo, JobExecutionContextImpl executionContext){
        this.jobExecutionId = jobExecutionId;
        this.jobTypeInfo = jobTypeInfo;
        this.executionContext = executionContext;
    }

    public JobExecutionId getId() {
        return jobExecutionId;
    }

    public JobTypeInfo getTypeInfo() {
        return jobTypeInfo;
    }

    @Override
    public boolean isInterruptionRequested() {
        return executionContext.isInterruptionRequested();
    }

    @Override
    public void interrupt() {
        this.executionContext.requestInterruption();
    }

    @Override
    public boolean isRunning() {
        return this.running;
    }

    void executionComplete(){
        this.running = false;
    }

}
