package org.emerjoin.commons.jobs.impl.cdi;

import java.util.Optional;
import java.util.UUID;

public class CdiJobExecutionContextInstance {

    private String id;
    private ContextObjectsHolder holder = new ContextObjectsHolder();

    CdiJobExecutionContextInstance(){
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    void destroy(){
        this.holder.destroyAll();
    }


    Optional<ScopedInstance> getBeanInstance(Class beanClazz){
        return this.holder.get(beanClazz);
    }

    void putBeanInstance(Class type, ScopedInstance scopedInstance){
        this.holder.put(type,scopedInstance);
    }

    void putBeanInstance(ScopedInstance scopedInstance){

        this.holder.put(scopedInstance);

    }

}
